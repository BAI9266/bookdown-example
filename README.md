# How to publish a book on GitLab with bookdown.

This is a minimal example of a book based on R Markdown and
**bookdown**, that can be hosted and edited on GitLab.

## How to start your own book

1. Fork this project on GitLab. It will be automatically published on
   the address `https://<username>.gitlab.io/bookdown-example/` (but
   you may have to wait for a few minutes). For more details on how
   this works check the next section.

2. Go to the "Settings" of the project and change the "Project
   name". Expand the "Advanced" section and change the project path as
   well.
   
3. Edit `index.Rmd` and change any fields, like: `title:`, `author:`,
   `date:`, `url:` and `description:`.
   
4. Edit `_bookdown.yml` and change the field `book_filename:`.
   
5. Change and modify the `*.md` files, and don't forget to update
   `_bookdown.yml` as well with the names of the new files.
   
6. Optionally, you may also experiment with changing the look and
   layout of the book by customizing `index.Rmd`. For more details
   about the available options make sure to read:
   - https://bookdown.org/yihui/bookdown/html.html
   - https://bookdown.org/yihui/bookdown/yaml-options.html
 
## How the book is auto-published on GitLab

You have already seen (in the previous section) that whenever you make
changes to the GitLab project
`https://gitlab.com/<username>/<book-title>.git`, the book is
automatically published and updated on the URL address
`https://<username>.gitlab.io/<book-title>/`. How does it happen?

The key to understanding this is the file `.gitlab-ci.yml`, which has
a content like this:

```yml
image: dockerscripts/bookdown

pages:
  script:
  - ./render.sh
  artifacts:
    paths:
    - public
  only:
  - master
```

This file instructs the CI/CD (Continuous Integration and Development)
system of GitLab to do something whenever the content of the project
is updated (for example some commits are pushed or a _Merge Request_
is accepted).

The line `image: dockerscripts/bookdown` tells it to start a Docker
container based on the image
[dockerscripts/bookdown](https://hub.docker.com/r/dockerscripts/bookdown).

The field `pages:` tells it that this CI/CD job is about generating
the GitLab pages for this project. The content of the field shows how
to do it. Basically, in plain English, it says something like this:
- Clone the branch `master` of the project inside the Docker
  container,
- then run the script `./render.sh` in it (which is expected to
  generate the directory `public/` with the content of the website
  that should be published),
- then get the content of the directory `public/` and publish it on
  the website of the project:
  `https://<username>.gitlab.io/<book-title>/`

You can check the page "CI/CD -> Pipelines" to see more details about
the CI/CD jobs, whether they passed or failed, etc. This can be very
useful especially when a job has failed, because you can also check
the log messages and hopefully find out what was the problem.

## How to add a custom domain

If you want to use a custom domain for the book you can do this from
"Settings -> Pages".

For example for the book
http://dashohoxha.gitlab.io/101-problema-programimi/ I have used as
custom domain http://p101.fs.al, which is much shorter and convenient.

## How to render and test the book locally

The details are described on:
https://gitlab.com/docker-scripts/bookdown

Let's summarize them quickly:

1. Install [docker-scripts](https://gitlab.com/docker-scripts/ds#installation):
   ```bash
   apt install m4
   git clone https://gitlab.com/docker-scripts/ds /opt/docker-scripts/ds
   cd /opt/docker-scripts/ds/
   make install
   ```

2. Install [wsproxy](https://gitlab.com/docker-scripts/wsproxy#installation):
   ```bash
   ds pull wsproxy
   ds init wsproxy @wsproxy
   cd /var/ds/wsproxy/
   ds make
   ```
   
3. Install [bookdown](https://gitlab.com/docker-scripts/bookdown) container:
   ```bash
   ds pull bookdown
   ds init bookdown @bookdown
   cd /var/ds/bookdown/
   ds make
   ```
   
4. Install the book
   [bookdown-example](https://gitlab.com/dashohoxha/bookdown-example):
   ```bash
   cd /var/ds/bookdown/
   git clone https://gitlab.com/dashohoxha/bookdown-example
   mv bookdown-example books/
   ds render books/bookdown-example
   ds site add mybook.example.org books/bookdown-example
   ```
   
5. Add on `/etc/hosts` this line:
   ```
   127.0.0.1 mybook.example.org
   ```
   Then open in browser: `https://mybook.example.org`.
   
You can edit `books/bookdown-example/` with any editor that you
like. Then, to update the page with the latest changes you run again
`ds render books/bookdown-example/`. This will regenerate the content
of the directory `books/bookdown-example/public/`, which is being
served on `https://mybook.example.org`.

Alternatively, you can update/regenerate it like this:
```bash
ds shell
cd books/bookdown-example/
./render.sh
```

## How to publish the book on GitHub

Since GitHub pages support only Jekyll and plain HTML pages, the most
easy way to publish a bookdown book on GitHub is to generate the HTML
pages locally, as described on the previous section, and to push them
on GitHub. You can do it like this:

1. Edit the script `books/bookdown-example/render.sh` and add to it a
   command that creates an empty file named `.nojekyll` on the
   `public/` directory:
   
   ```bash
   echo 'touch public/.nojekyll' >> books/bookdown-example/render.sh
   ```
   
   This will let GitHub know that the content of the `public/`
   directory contains a plain HTML site, and it should not try to
   process it with Jekyll.

2. Regenerate the `public/` directory which contains the HTML files and
   other files that compose the website of the book:
   
   ```bash
   ds render books/bookdown-example/
   ```
   
3. Remove the file `.gitignore` which prevents the directory `public/`
   from being committed to Git, and commit this directory to Git:
   
   ```bash
   cd books/bookdown-example/
   cat .gitignore
   git rm .gitignore
   git add public
   git commit -m 'Add rendered book'
   git push
   ```
   
4. Go to your repository’s settings and under "GitHub Pages" change
   the "Source" to be "master branch /public folder" (as described in
   this [GitHub help
   page](https://help.github.com/en/github/working-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site)).
   
To update the page, after making some changes, you have to regenerate
the `public/` directory with `ds render books/bookdown-example/`, and
to commit and push its changes to GitHub.

It is also possible to build an auto-update system with Travis CI,
which would update the website of the book whenever some changes to
the markdown files are committed and pushed to GitHub. This automated
way is described in this [help
page](https://bookdown.org/yihui/bookdown/github.html). However it is
a bit complex and maybe not worth the trouble.

## The syntax of the markdown files

The format of the `*.md` and `*.Rmd` files is basically the Pandoc's
Markdown syntax, which is described on this page as well:
https://bookdown.org/yihui/bookdown/markdown-syntax.html

However bookdown supports some further extensions, which are mainly
useful for math. These extensions are described on this page:
https://bookdown.org/yihui/bookdown/markdown-extensions-by-bookdown.html

Besides these, the syntax of GitLab markdown is a bit different from
that of Pandoc's Markdown. For example the delimiter of the math code
on Pandoc's Markdown is a single dollar sign, like this: `$a^2$`,
while that delimiter for GitLab Markdown is like this: ``$`a^2`$``.
These small differences are fixed automatically by the script
`render.sh`, so that markdown files are rendered nicely both on the
GitLab web interface and on the website of the book.

## Some tips and tricks

If you have any problems, check also these [tips and tricks](https://gitlab.com/dashohoxha/bookdown-example/-/wikis/Tips-and-Tricks) suggested by the community.
